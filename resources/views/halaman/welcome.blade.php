@extends('layout.master')

@section('judul')
    Halaman Index
@endsection

@section('content')
    <h1>SELAMAT DATANG! {{$namadepan}} {{$namabelakang}}</h1>
    <h3>Terima Kasih telah bergabung di Website Kami. Media Belajar kita bersama!</h3>
@endsection
@extends('layout.master')

@section('judul')
    Halaman Form
@endsection

@section('content')
    <h3>Buat Account Baru</h3>
    <h4>Sign Up Form</h4>
    <form action="/welcome" method="post">
        @csrf
        <label for="">First Name:</label> <br><br>
        <input type="text" name="namadepan">
        <br>
        <label for="">Last Name:</label> <br><br>
        <input type="text" name="namabelakang">
        <br><br>
        <label for="">Gender</label> <br><br>
        <input type="radio" name="gender"> Male <br>
        <input type="radio" name="gender"> Female <br><br>
        <label for="">Nationality</label> <br><br>
        <select name="nationality" id="">
            <option value="7">Indonesia</option>
            <option value="8">Malaysia</option>
            <option value="9">Singapura</option>
        </select>
        <br><br>
        <label for="">Language Spoken</label><br><br>
        <input type="checkbox" value="1" name="bahasa" id="">Bahasa Indonesia <br>
        <input type="checkbox" value="2" name="bahasa" id="">English <br>
        <input type="checkbox" value="3" name="bahasa" id="">Other <br><br>
        <label for="">Bio</label><br>
        <textarea name="bio" id="" cols="30" rows="10"></textarea><br><br>
        <input type="submit" value="Sign Up">
    </form>
@endsection